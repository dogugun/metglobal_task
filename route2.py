import sys

from pyspark import SparkContext

if len(sys.argv) < 2:
    print("Wrong parameters, default paths will be used")
    input_path="route_output"
    output_path="route_output_average"
else:
    input_path = sys.argv[1]
    output_path = sys.argv[2]

sc = SparkContext(appName="Route_2")
from pyspark.sql import SQLContext
sqlContext = SQLContext(sc)

#df = sqlContext.read.format("com.databricks.spark.csv").option("header", "true").load("/Users/dogugunozkaya/datasets/route_output/part*")
df = sqlContext.read.format("com.databricks.spark.csv").option("header", "true").load(input_path + "/part*")
df_grouped = df.groupby('hotel_code').agg({"cost": "avg"})

#df_grouped.show()
df_grouped.write.format("com.databricks.spark.csv").option("header", "true").save(output_path)