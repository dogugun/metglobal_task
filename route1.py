import sys

from pyspark import SparkContext



if len(sys.argv) < 2:
    print("Wrong parameters, default paths will be used")
    input_path="hotels_search.log"
    output_path="route_output"
else:
    input_path = sys.argv[1]
    output_path = sys.argv[2]

sc = SparkContext(appName="Route_1")
from pyspark.sql import SQLContext
sqlContext = SQLContext(sc)

#df = sqlContext.read.json('/Users/dogugunozkaya/datasets/hotels_search.log')
df = sqlContext.read.json(input_path)

#df.registerTempTable("df")
#df.printSchema()

df_product = df.select("products")

#provider,
# hotel_code,
# room_type,
# room_category,
# meal_type



df_arr_fields = df_product.select("products.providers", "products.hotel_code", "products.rooms", "products.meal_type", "products.cost")

from pyspark.sql.functions import explode
df_arr_fields = df_arr_fields.withColumn("provider", explode(df_arr_fields["providers"]))
df_arr_fields = df_arr_fields.withColumn("provider1", explode(df_arr_fields["provider"]))
df_arr_fields = df_arr_fields.drop("providers")
df_arr_fields = df_arr_fields.drop("provider")
df_arr_fields = df_arr_fields.withColumnRenamed("provider1", "provider")
df_arr_fields = df_arr_fields.withColumn("hotel_code1", explode(df_arr_fields["hotel_code"]))
df_arr_fields = df_arr_fields.drop("hotel_code")
df_arr_fields = df_arr_fields.withColumnRenamed("hotel_code1", "hotel_code")
df_arr_fields = df_arr_fields.withColumn("room1", explode(df_arr_fields["rooms"]))
df_arr_fields = df_arr_fields.withColumn("room2", explode(df_arr_fields["room1"]))
df_arr_fields = df_arr_fields.withColumn("room_type", df_arr_fields["room2"]["room_type"])
df_arr_fields = df_arr_fields.withColumn("room_category", df_arr_fields["room2"]["room_category"])
df_arr_fields = df_arr_fields.drop("rooms")
df_arr_fields = df_arr_fields.drop("room1")
df_arr_fields = df_arr_fields.drop("room2")
df_arr_fields = df_arr_fields.withColumn("meal_type1", explode(df_arr_fields["meal_type"]))
df_arr_fields = df_arr_fields.withColumn("cost1", explode(df_arr_fields["cost"]))
df_arr_fields = df_arr_fields.drop("meal_type")
df_arr_fields = df_arr_fields.drop("cost")
df_arr_fields = df_arr_fields.withColumnRenamed("meal_type1", "meal_type")
df_arr_fields = df_arr_fields.withColumnRenamed("cost1", "cost")

#df_arr_fields.show()


#df_arr_fields = df_arr_fields.limit(1000)

df_arr_fields.write.format("com.databricks.spark.csv").option("header", "true").save(output_path)