# Metglobal Recruitment Task



The program consists of 2 parts.

 * route1
 * route2

Both parts are run from Spark installation with the following command:

```terminal
./bin/spark-submit --packages com.databricks:spark-csv_2.10:1.4.0 <pathToScript>/route<x>.py <inputPath> <outputPath>
```
Note: com.databricks:spark-csv_2.10:1.4.0 package is used for csv operations.

## route1
This script takes 2 parameters. One is for input and one is for output. If no parameters are set, default values will be used. 

Default values:
 * Input: hotels_search.log
 * Output: route_output


This part gets the dataset which is in json form and manipulates it into te form with columns:
 * provider
 * hotel_code
 * room_type
 * room_category
 * meal_type
 * cost

This ungrouped data is written in csv format to output path, which is set on parameters. 

## route2
This script takes 2 parameters. One is for input and one is for output. If no parameters are set, default values will be used. 

Default values:
 * Input: route_output
 * Output: route_output_average


This script takes the ungrouped data created in route1 and gets average based on hotel as asked in the task. The result is saved into output path in csv format. The data fields will be:
 * hotel_code
 * avg

